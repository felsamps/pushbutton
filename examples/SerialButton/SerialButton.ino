#include <PushButton.h>

PushButton btn1(8);

void setup() {
  Serial.begin(9600);
}

void loop() {
  if(btn1.pressionadoPorNivel()) {
    Serial.println("Botão pressionado por nível");
  }

  if(btn1.pressionadoPorPulso()) {
    Serial.println("Botão pressionado por pulso");
  }

}
