#ifndef PUSH_BUTTON_H
#define PUSH_BUTTON_H

#include <inttypes.h>
#include <Print.h>
#include <Arduino.h>

class PushButton {
  private:
    int pino;
    int estadoAtual, ultimoEstado;

  public:

    PushButton(int pino);

    bool pressionadoPorPulso();
    bool pressionadoPorNivel();
};

#endif
